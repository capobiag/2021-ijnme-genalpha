# PyNSGA

This directory provides the files for reproducing the numerical examples of the paper

>**A nonsmooth generalized-alpha method for mechanical systems with frictional contact**
>
>Giuseppe Capobianco, Jonas Harsch, Simon R. Eugster and Remco I. Leine
>
>*Int J Numer Methods Eng. 2021; 122: 6497– 6526. https://doi.org/10.1002/nme.6801*

It consists of the **PyNSGA** package, which contains an implementation of the nonsmooth generalized-alpha method presented in the paper and an implementation of Moreau's time stepping scheme. These schemes are used to simulate the example systems 
- Rotating bouncing ball (`bouncing_ball.py`)
- Ball in corner (`ball_in_corner.py`)
- Ball in cylinder (`ball_in_cylinder.py`)
- Painlevè rod (`Painleve.py`)
- Tippetop (`tippetop.py`)
  
found in the **examples** directory.

## Installation

Download or [clone](https://git-scm.com/docs/git-clone) this directory. Navigate to the downloaded directory and install the **PyNSGA** package via

```bash
python -m pip install .
```

## Run an example

An example system can be simulated by running the corresponding file, e.g. the rotating bouncing ball is simulated by calling

```bash
python examples/bouncing_ball/bouncing_ball.py
```

## Visualize using blender

Using the free and open source visualization tool [blender](https://www.blender.org/) all presented examples can be visualized, e.g. the rotating bouncing ball is animated by calling

```bash
blender --python examples/bouncing_ball/animate_bouncing_ball.py
```

The animations have been tested with blender version 2.92.0.