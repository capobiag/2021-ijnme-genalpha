import bpy
import numpy as np
import os

def remove_inital_cube():
    """Removes initial cube of the scene."""
    bpy.data.objects.remove(bpy.context.scene.objects['Cube'], do_unlink=True)

def clear_scene():
    """Removes everything from the scene."""
    bpy.ops.wm.read_factory_settings(use_empty=True)

def alpha_background():
    # set white brackground, see  https://www.youtube.com/watch?v=aegiN7XeLow
    bpy.context.scene.render.film_transparent = True
    bpy.context.scene.view_settings.view_transform = 'Standard'
    
    # node tree
    bpy.context.scene.use_nodes = True
    tree = bpy.context.scene.node_tree

    # move existing 'Composition' node and remove link
    composition_node = bpy.context.scene.node_tree.nodes['Composite']
    composition_node.location = (composition_node.location[0] + 500, composition_node.location[1])
    l = composition_node.inputs[0].links[0]
    tree.links.remove(l)

    # add alpha over node
    alpha_node = tree.nodes.new(type='CompositorNodeAlphaOver')
    alpha_node.location = (500, 300)
    alpha_node.premul = 1

    # link nodes
    links = tree.links
    link = links.new(bpy.context.scene.node_tree.nodes['Render Layers'].outputs[0], alpha_node.inputs[2])
    link = links.new(alpha_node.outputs[0], composition_node.inputs[0])


def use_eevee():
    # use cycles with GPU render and some qualiy setups
    bpy.context.scene.render.engine = 'BLENDER_EEVEE'

def toggle_camera_view():
    area = next(area for area in bpy.context.screen.areas if area.type == 'VIEW_3D')
    area.spaces[0].region_3d.view_perspective = 'CAMERA'

def set_view_point_shader(mode='RENDERED'):
    my_areas = bpy.context.workspace.screens[0].areas
    for area in my_areas:
        for space in area.spaces:
            if space.type == 'VIEW_3D':
                space.shading.type = mode

def load_data(path, start=0, end=-1):
    """Load centerline, orientation and radius of python dictionary as numpy binary file."""
    data = np.load(path, allow_pickle=True)


    objects = data[()]['objects']
    r_OS = data[()]['r_OS']
    A_IK = data[()]['A_IK']
    t = data[()]['t']
    dt = data[()]['dt']

    # TODO:
    my_dict = {
        'objects': objects,
        'r_OS': r_OS,
        'A_IK': A_IK,
        't': t,
        # 'r_OS': r_OS[start:end],
        # 'A_IK': A_IK[start:end],
        # 't': t[start:end],
        'dt': dt,
    }

    return my_dict
    # return t[start:end], dt, r_OS[start:end], A_IK[start:end]

def rot2quat(R):
    """
    Compute unit quaterion from given rotation matrix.
    """
    qr = 0.5 * np.sqrt(1 + R[0, 0] + R[1, 1] + R[2, 2])
    fac = 1 / (4 * qr)
    qi = fac * (R[2, 1] - R[1, 2])
    qj = fac * (R[0, 2] - R[2, 0])
    qk = fac * (R[1, 0] - R[0, 1])
    return qr, qi, qj, qk

def build_rigid_bodies(bodylist, scale = 1):
    object_list = []
    for body in bodylist:
        bodypath = body
        bpy.ops.import_mesh.stl(filepath=bodypath, filter_glob="*.stl")
        obj = bpy.context.active_object
        object_list.append(obj)

        # scale objects dimensions correct
        # CAD assumes mm -> blender assumes m
        bpy.context.object.scale[0] = scale
        bpy.context.object.scale[1] = scale
        bpy.context.object.scale[2] = scale

    return object_list
    
def animate_rigid_bodies(bodies, r_OSs, A_IKs, scale=1):
    object_list = build_rigid_bodies(bodies, scale)

    for i, obj in enumerate(object_list):
        r_OS = r_OSs[i]
        A_IK = A_IKs[i]
        for frame in range(r_OSs[0].shape[0]):
            bpy.context.scene.frame_set(frame)
            obj.rotation_mode = 'QUATERNION'

            obj.location = r_OS[frame]
            obj.rotation_quaternion = rot2quat(A_IK[frame].T)

            obj.keyframe_insert(data_path="location")
            obj.keyframe_insert(data_path="rotation_quaternion")

    # set initial and end frame
    bpy.context.scene.frame_current = 0
    bpy.context.scene.frame_end = frame