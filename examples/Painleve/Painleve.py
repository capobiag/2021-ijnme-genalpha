#-----------------------------------------------------------------------
# A nonsmooth generalized-alpha method for mechanical systems
# with frictional contact
# 
# Giuseppe Capobianco, Jonas Harsch, Simon R. Eugster, Remco I. Leine
#-----------------------------------------------------------------------
# Int J Numer Methods Eng. 2021; 1– 30. https://doi.org/10.1002/nme.6801
#-----------------------------------------------------------------------
# 
# This file implements the Painlevé rod example, 
# see Section 10.4.
#
# Stuttgart, September 2021                      G.Capobianco, J. Harsch

import sys
from os import path
here = path.abspath(path.dirname(__file__))
sys.path.append(here)

import numpy as np
from math import cos, sin, pi

import matplotlib.pyplot as plt

from PyNSGA import Generalized_alpha, Moreau

class Painleve_rod():
    def __init__(self, mu=5/3, q0=None, u0=None):
        """Glocker1995, 5.3.4 Stoß ohne Kollision"""
        
        self.m = 1
        self.s = 1
        self.J_S = self.m * self.s**2 / 3
        self.g_ = 10

        self.mu = np.array([mu])
        self.e_N = np.array([0])
        self.e_F = np.array([0])

        self.prox_r_N = np.array([0.1])
        self.prox_r_F = np.array([0.1])

        self.NF_connectivity = [[0]]

        x0 = 0
        phi0 = 31 / 180 * pi
        y0 = sin(phi0) * self.s # 0.5150380749100542

        x_dot0 = 30
        y_dot0 = 0
        phi_dot0 = 0

        self.nq = 3
        self.nu = 3
        self.nla_g = 0
        self.nla_gamma = 0
        self.nla_N = 1
        self.nla_F = 1

        self.q0 = np.array([x0, y0, phi0]) if q0 is None else q0
        self.u0 = np.array([x_dot0, y_dot0, phi_dot0]) if u0 is None else u0
        self.la_g0 = np.zeros(self.nla_g)
        self.la_gamma0 = np.zeros(self.nla_gamma)
        self.la_N0 = np.zeros(self.nla_N)
        self.la_F0 = np.zeros(self.nla_F)
    
    def r_OS(self, t, q):
        r_OS = np.zeros(3)
        r_OS[:2] = q[:2]
        return r_OS

    def A_IK(self, t, q):
        sp = sin(q[2])
        cp = cos(q[2])
        return np.array([[cp, -sp, 0],
                         [sp,  cp, 0],
                         [ 0,   0, 1]])

    #####################
    # equations of motion
    #####################
    def M(self, t, q):
        return np.diag(np.array([self.m, self.m, self.J_S]))

    def h(self, t, q, u):
        return np.array([0, - self.m * self.g_, 0])

    #####################
    # kinematic equations
    #####################
    def q_dot(self, t, q, u):
        return u

    def q_ddot(self, t, q, u, u_dot):
        return u_dot

    def B(self, t, q):
        return np.eye(3)

    #######################
    # bilateral constraints
    #######################
    def g(self, t, q):
        return np.zeros(self.nla_g)

    def W_g(self, t, q):
        return np.zeros((self.nu, self.nla_g))

    def g_dot(self, t, q, u):
        return np.zeros(self.nla_g)

    def g_ddot(self, t, q, u, a):
        return np.zeros(self.nla_g)

    def gamma(self, t, q, u):
        return np.zeros(self.nla_gamma)

    def W_gamma(self, t, q):
        return np.zeros((self.nu, self.nla_gamma))

    def gamma_dot(self, t, q, u, a):
        return np.zeros(self.nla_gamma)

    #################
    # normal contacts
    #################
    def g_N(self, t, q):
        x, y, phi = q
        return np.array([y - self.s * sin(phi)])

    def g_N_dot(self, t, q, u):
        x, y, phi = q
        x_dot, y_dot, phi_dot = u
        return np.array([y_dot - self.s * cos(phi) * phi_dot])

    def g_N_dot_u_dense(self, t, q):
        x, y, phi = q
        return np.array([[0, 1, -self.s * cos(phi)]])
    
    def xi_N(self, t, q, u_pre, u_post):
        return self.g_N_dot(t, q, u_post) + self.e_N * self.g_N_dot(t, q, u_pre)

    def W_N(self, t, q):
        return self.g_N_dot_u_dense(t, q).T

    def g_N_ddot(self, t, q, u, u_dot):
        x, y, phi = q
        x_dot, y_dot, phi_dot = u
        x_ddot, y_ddot, phi_ddot = u_dot
        return np.array([y_ddot + self.s * sin(phi) * phi_dot**2 - self.s * cos(phi) * phi_ddot])

    #################
    # friction
    #################
    def gamma_F(self, t, q, u):
        x, y, phi = q
        x_dot, y_dot, phi_dot = u
        return np.array([x_dot - self.s * sin(phi) * phi_dot])

    def gamma_F_u_dense(self, t, q):
        x, y, phi = q
        return np.array([[1, 0, -self.s * sin(phi)]])

    def W_F(self, t, q):
        return self.gamma_F_u_dense(t, q).T

    def gamma_F_dot(self, t, q, u, u_dot):
        x, y, phi = q
        x_dot, y_dot, phi_dot = u
        x_ddot, y_ddot, phi_ddot = u_dot
        return np.array([x_ddot - self.s * cos(phi) * phi_dot**2 - self.s * sin(phi) * phi_ddot])

    def xi_F(self, t, q, u_pre, u_post):
        return self.gamma_F(t, q, u_post) + self.e_F * self.gamma_F(t, q, u_pre)

if __name__ == "__main__":
    # create output data for plots in paper
    output_paper = True
    
    # create output data for blender visualization
    output_blender = True
    
    # show simulation results
    show_plots = True

    rod = Painleve_rod()

    t0 = 0
    t1 = 1.5
    dt = 8e-4

    gen_al = Generalized_alpha(rod, t0, t1, dt, rho_inf=0.9, newton_tol=1.0e-8, newton_max_iter=100)
    sol_g = gen_al.solve()

    t_g = sol_g[0]
    q_g = sol_g[1]
    u_g = sol_g[2]
    a_g = sol_g[3]
    La_N_g = sol_g[10]
    la_N_g = sol_g[11]
    La_F_g = sol_g[12]
    la_F_g = sol_g[13]
    P_N_g = sol_g[14]
    P_F_g = sol_g[15]
        
    nt = len(t_g)

    if output_blender:
        r_OS = np.zeros((nt, 3))
        A_IK = np.zeros((nt, 3, 3))

        for i, (ti, qi) in enumerate(zip(t_g, q_g)):
            r_OS[i] = rod.r_OS(ti, qi)
            A_IK[i] = rod.A_IK(ti, qi)

        n_frames = 200
        frames = np.linspace(0, nt - 1, num=n_frames, dtype=int)

        my_dict = {
            'objects': [path.join(here, 'Painleve.stl')],
            'r_OS': [r_OS[frames]],
            'A_IK': [A_IK[frames]],
            't': t_g[frames],
            'dt': dt,
        }

        np.save(path.join(here, 'Painleve.npy'), my_dict, allow_pickle=True)

    if output_paper:
        txt_data = np.vstack([t_g[0::10], u_g[0::10, 2]])
        np.savetxt(path.join(here, 'Painleve_rod_phi_dot.txt'), txt_data.T)

    if show_plots:
        rod.prox_r_N = np.array([0.4])
        rod.prox_r_T = np.array([0.4])
        moreau = Moreau(rod, t0, t1, dt, fix_point_tol=1e-6)
        sol_m = moreau.solve()
        t_m = sol_m[0]
        q_m = sol_m[1]
        u_m = sol_m[2]
        P_N_m = sol_m[5]
        P_F_m = sol_m[6]

        for k in range(2):
            t_a = 0.855
            if k==0:
                t = t_g
                q = q_g
                u = u_g
                title = 'gen. alpha'
            else:
                t = t_m
                q = q_m
                u = u_m
                title = 'moreau'

            # positions
            fig, ax = plt.subplots(3, 3)
            fig.suptitle(title)
            ax[0, 0].set_xlabel('t [s]')
            ax[0, 0].set_ylabel('x [m]')
            ax[0, 0].plot(t, q[:, 0], '-k')
            ax[0, 0].plot([t_a, t_a], [min(q[:, 0]), max(q[:, 0])], '--k')

            ax[0, 1].set_xlabel('t [s]')
            ax[0, 1].set_ylabel('y [m]')
            ax[0, 1].plot(t, q[:, 1], '-k')
            ax[0, 1].plot([t_a, t_a], [min(q[:, 1]), max(q[:, 1])], '--k')

            ax[0, 2].set_xlabel('t [s]')
            ax[0, 2].set_ylabel('phi [rad]')
            ax[0, 2].plot(t, q[:, 2], '-k')
            ax[0, 2].plot([t_a, t_a], [min(q[:, 2]), max(q[:, 2])], '--k')

            # velocities
            ax[1, 0].set_xlabel('t [s]')
            ax[1, 0].set_ylabel('x_dot [m/s]')
            ax[1, 0].plot(t, u[:, 0], '-k')
            ax[1, 0].plot([t_a, t_a], [min(u[:, 0]), max(u[:, 0])], '--k')

            ax[1, 1].set_xlabel('t [s]')
            ax[1, 1].set_ylabel('y_dot [m/s]')
            ax[1, 1].plot(t, u[:, 1], '-k')
            ax[1, 1].plot([t_a, t_a], [min(u[:, 1]), max(u[:, 1])], '--k')

            ax[1, 2].set_xlabel('t [s]')
            ax[1, 2].set_ylabel('phi_dot [rad/s]')
            ax[1, 2].plot(t, u[:, 2], '-k')
            ax[1, 2].plot([t_a, t_a], [min(u[:, 2]), max(u[:, 2])], '--k')

            # gaps
            nt = len(t)
            g_N = np.zeros(nt)
            g_N_dot = np.zeros(nt)
            gamma_T = np.zeros(nt)

            for i, ti in enumerate(t):
                g_N[i] = rod.g_N(ti, q[i])
                g_N_dot[i] = rod.g_N_dot(ti, q[i], u[i])
                gamma_T[i] = rod.gamma_F(ti, q[i], u[i])

            ax[2, 0].set_xlabel('t [s]')
            ax[2, 0].set_ylabel('g_N [m]')
            ax[2, 0].plot(t, g_N, '-k')
            ax[2, 0].plot([t_a, t_a], [min(g_N), max(g_N)], '--k')

            ax[2, 1].set_xlabel('t [s]')
            ax[2, 1].set_ylabel('g_N_dot [m/s]')
            ax[2, 1].plot(t, g_N_dot, '-k')
            ax[2, 1].plot([t_a, t_a], [min(g_N_dot), max(g_N_dot)], '--k')

            ax[2, 2].set_xlabel('t [s]')
            ax[2, 2].set_ylabel('gamma_T [m/s]')
            ax[2, 2].plot(t, gamma_T, '-k')
            ax[2, 2].plot([t_a, t_a], [min(gamma_T), max(gamma_T)], '--k')

        plt.show()

    