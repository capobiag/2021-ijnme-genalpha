#-----------------------------------------------------------------------
# A nonsmooth generalized-alpha method for mechanical systems
# with frictional contact
# 
# Giuseppe Capobianco, Jonas Harsch, Simon R. Eugster, Remco I. Leine
#-----------------------------------------------------------------------
# Int J Numer Methods Eng. 2021; 1– 30. https://doi.org/10.1002/nme.6801
#-----------------------------------------------------------------------
# 
# This file implements the ball in corner example, 
# see Section 10.2.
#
# Stuttgart, September 2021                      G.Capobianco, J. Harsch

import sys
from os import error, path
here = path.abspath(path.dirname(__file__))
sys.path.append(here)

import numpy as np
from math import sin, cos, pi
import matplotlib.pyplot as plt

from PyNSGA import Generalized_alpha, Moreau

def A_IK_basic_z(phi):
    sp = sin(phi)
    cp = cos(phi)
    return np.array([[ cp, -sp, 0],\
                     [ sp,  cp, 0],\
                     [  0,   0, 1]])

class Ball_in_corner():
    def __init__(self, eN1, eN2, mu1, mu2, x0, y0, phi0, x_dot0, y_dot0, phi_dot0):

        # plane angles
        self.alpha1 = pi/4
        self.alpha2 = pi/4
        
        self.n1 = np.array([ -sin(self.alpha1), cos(self.alpha1), 0])
        self.t1 = np.array([  cos(self.alpha1), sin(self.alpha1), 0])

        self.n2 = np.array([ sin(self.alpha2),  cos(self.alpha2), 0])
        self.t2 = np.array([ cos(self.alpha2), -cos(self.alpha2), 0])
        
        self.m = 1
        self.R = 0.1
        self.theta = 2 / 5 * self.m * self.R**2 
        self.g_ = 9.81
        
        self.nq = 3
        self.nu = 3
        self.nla_g = 0
        self.nla_gamma = 0
        self.nla_N = 2
        self.nla_F = 2

        eF = 0
        self.mu = np.array([mu1, mu2])
        self.e_N = np.array([eN1, eN2])
        self.e_F = eF * np.ones(self.nla_F)

        prox_r = 0.2
        self.prox_r_N = prox_r * np.ones(self.nla_N)
        self.prox_r_F = prox_r * np.ones(self.nla_N)

        self.NF_connectivity = [[0], [1]]

        # Initial conditions
        self.q0 = np.array([x0, y0, phi0])
        self.u0 = np.array([x_dot0, y_dot0, phi_dot0])

        self.la_g0 = np.zeros(self.nla_g)
        self.la_gamma0 = np.zeros(self.nla_gamma)

        self.la_N0 = np.zeros(self.nla_N)
        self.la_F0 = np.zeros(self.nla_F)

    def r_OS(self, t, q):
        r_OS = np.zeros(3)
        r_OS[:2] = q[:2]
        return r_OS

    def A_IK(self, t, q):
        return A_IK_basic_z(q[2])

    #####################
    # equations of motion
    #####################
    def M(self, t, q):
        return np.diag([self.m, self.m, self.theta])

    def h(self, t, q, u):
        return np.array([0,
                         - self.m * self.g_,
                         0])

    #####################
    # kinematic equations
    #####################
    def q_dot(self, t, q, u):
        return u

    def q_ddot(self, t, q, u, a):
        return a

    def B(self, t, q):
        return np.eye(self.nu)

    #######################
    # bilateral constraints
    #######################
    def g(self, t, q):
        return np.zeros(self.nla_g)

    def W_g(self, t, q):
        return np.zeros((self.nu, self.nla_g))

    def g_dot(self, t, q, u):
        return np.zeros(self.nla_g)

    def g_ddot(self, t, q, u, a):
        return np.zeros(self.nla_g)

    def gamma(self, t, q, u):
        return np.zeros(self.nla_gamma)

    def W_gamma(self, t, q):
        return np.zeros((self.nu, self.nla_gamma))

    def gamma_dot(self, t, q, u, a):
        return np.zeros(self.nla_gamma)

    #################
    # normal contacts
    #################
    def g_N(self, t, q):
        r_OS = self.r_OS(t, q)
        return np.array([ r_OS @ self.n1 - self.R , r_OS @ self.n2 - self.R])

    def W_N(self, t, q):
        W_N = np.zeros((self.nu, self.nla_N))
        W_N[: , 0] = self.n1
        W_N[: , 1] = self.n2
        return W_N

    def g_N_dot(self, t, q, u):
        v_S = self.r_OS(t, u)
        return np.array([ v_S @ self.n1 , v_S @ self.n2])
    
    def xi_N(self, t, q, u_pre, u_post):
        return self.g_N_dot(t, q, u_post) + self.e_N * self.g_N_dot(t, q, u_pre)

    def g_N_ddot(self, t, q, u, a):
        a_S = self.r_OS(t, a)
        return np.array([ a_S @ self.n1 , a_S @ self.n2])

    #################
    # friction
    #################
    def gamma_F(self, t, q, u):
        phi_dot = u[2]
        v_S = self.r_OS(t, u)
        return np.array([ v_S @ self.t1 + self.R * phi_dot, v_S @ self.t2 + self.R * phi_dot])

    def W_F(self, t, q):
        W_F = np.zeros((self.nu, self.nla_F))
        W_F[:2, 0] = self.t1[:2]
        W_F[2, 0]  = self.R
        W_F[:2, 1] = self.t2[:2]
        W_F[2, 1]  = self.R
        return W_F

    def gamma_F_dot(self, t, q, u, a):
        phi_ddot = a[2]
        a_S = self.r_OS(t, a)
        return np.array([ a_S @ self.t1 + self.R * phi_ddot, a_S @ self.t2 + self.R * phi_ddot])

    def xi_F(self, t, q, u_pre, u_post):
        return self.gamma_F(t, q, u_post) + self.e_F * self.gamma_F(t, q, u_pre)
    
if __name__ == "__main__":
    # create output data for plots in paper
    output_paper = True
    
    # create output data for blender visualization
    output_blender = True
    
    # show simulation results
    show_plots = True

    # system parameters
    eN1 = 0.5
    eN2 = 0
    mu1 = 0.3
    mu2 = 0.3

    # initial conditions
    x0 = -0.5
    y0 = 1
    phi0 = 0
    x_dot0 = 0
    y_dot0 = 0
    phi_dot0 = 0

    ball = Ball_in_corner(eN1, eN2, mu1, mu2, x0, y0, phi0, x_dot0, y_dot0, phi_dot0)

    t0 = 0
    t1 = 1.5
    dt = 1e-4
    gen_al = Generalized_alpha(ball, t0, t1, dt, rho_inf=0.5, method='newton', newton_tol=1.0e-6, newton_max_iter=100)
    sol_g = gen_al.solve()

    t_g = sol_g[0]
    q_g = sol_g[1]
    u_g = sol_g[2]
    a_g = sol_g[3]
    La_N_g = sol_g[10]
    la_N_g = sol_g[11]
    La_F_g = sol_g[12]
    la_F_g = sol_g[13]
    P_N_g = sol_g[14]
    P_F_g = sol_g[15]

    if show_plots:
        moreau = Moreau(ball, t0, t1, dt, fix_point_tol=1e-6)
        sol_m = moreau.solve()
        t_m = sol_m[0]
        q_m = sol_m[1]
        u_m = sol_m[2]
        P_N_m = sol_m[5]
        P_F_m = sol_m[6]

    nt = len(t_g)
    if output_blender:
        r_OS = np.zeros((nt, 3))
        A_IK = np.zeros((nt, 3, 3))

        for i, (ti, qi) in enumerate(zip(t_g, q_g)):
            r_OS[i] = ball.r_OS(ti, qi) 
            A_IK[i] = ball.A_IK(ti, qi)

        n_frames = 200
        frames = np.linspace(0, nt - 1, num=n_frames, dtype=int)

        my_dict = {
            'objects': [path.join(here, 'Ball.stl')],
            'r_OS': [r_OS[frames]],
            'A_IK': [A_IK[frames]],
            't': t_g[frames],
            'dt': dt,
        }

        np.save(path.join(here, 'Ball_in_corner.npy'), my_dict, allow_pickle=True)
    
    g_N = np.zeros((nt, 2))
    for i, (ti, qi) in enumerate(zip(t_g, q_g)):
        g_N[i] = ball.g_N(ti, qi)

    if output_paper:
        txt_data = np.vstack([t_g[0::10], g_N[0::10, 0]])
        np.savetxt(path.join(here, 'ball_in_corner_gap1.txt'), txt_data.T)
        txt_data = np.vstack([t_g[0::10], g_N[0::10, 1]])
        np.savetxt(path.join(here, 'ball_in_corner_gap2.txt'), txt_data.T)

    if show_plots:
        ######
        fig, ax = plt.subplots(3, 1)

        ax[0].set_xlabel('$t$')
        ax[0].set_ylabel('$x$')
        ax[0].plot(t_g, q_g[:, 0], '-k', label='gen_alpha')
        ax[0].plot(t_m, q_m[:, 0], '--r', label='moreau')
        ax[0].legend()

        ax[1].set_xlabel('$t$')
        ax[1].set_ylabel('$u_x$')
        ax[1].plot(t_g, u_g[:, 0], '-k', label='gen_alpha')
        ax[1].plot(t_m, u_m[:, 0], '--r', label='moreau')
        ax[1].legend()

        ax[2].set_xlabel('$t$')
        ax[2].set_ylabel('$a_x$')
        ax[2].plot(t_g, a_g[:, 0], '-k', label='gen_alpha')
        ax[2].legend()
        
        ######
        fig, ax = plt.subplots(3, 1)

        ax[0].set_xlabel('$t$')
        ax[0].set_ylabel('$y$')
        ax[0].plot(t_g, q_g[:, 1], '-k', label='gen_alpha')
        ax[0].plot(t_m, q_m[:, 1], '--r', label='moreau')
        ax[0].legend()

        ax[1].set_xlabel('$t$')
        ax[1].set_ylabel('$u_y$')
        ax[1].plot(t_g, u_g[:, 1], '-k', label='gen_alpha')
        ax[1].plot(t_m, u_m[:, 1], '--r', label='moreau')
        ax[1].legend()

        ax[2].set_xlabel('$t$')
        ax[2].set_ylabel('$a_y$')
        ax[2].plot(t_g, a_g[:, 1], '-k', label='gen_alpha')
        ax[2].legend()
        
        ######
        fig, ax = plt.subplots(3, 1)

        ax[0].set_xlabel('$t$')
        ax[0].set_ylabel(r'$\varphi$')
        ax[0].plot(t_g, q_g[:, 2], '-k', label='gen_alpha')
        ax[0].plot(t_m, q_m[:, 2], '--r', label='moreau')
        ax[0].legend()

        ax[1].set_xlabel('$t$')
        ax[1].set_ylabel(r'$u_\varphi$')
        ax[1].plot(t_g, u_g[:, 2], '-k', label='gen_alpha')
        ax[1].plot(t_m, u_m[:, 2], '--r', label='moreau')
        ax[1].legend()

        ax[2].set_xlabel('$t$')
        ax[2].set_ylabel(r'$a_\varphi$')
        ax[2].plot(t_g, a_g[:, 2], '-k', label='gen_alpha')
        ax[2].legend()

        ######
        fig, ax = plt.subplots(2, 1)
        ax[0].set_title('force computed by gen-alpha')
        ax[0].set_xlabel('$t$')
        ax[0].set_ylabel('force')
        ax[0].plot(t_g, la_N_g[:, 0], '-b', label='$\lambda_N$')
        ax[0].plot(t_g, La_N_g[:, 0], '--r', label='$\Lambda_N$')
        ax[0].legend()

        ax[1].set_xlabel('$t$') 
        ax[1].set_ylabel('force')
        ax[1].plot(t_g, la_F_g[:, 0], '-b', label='$\lambda_F$')
        ax[1].plot(t_g, La_F_g[:, 0], '--r', label='$\Lambda_F$')
        ax[1].legend()

        ######
        fig, ax = plt.subplots(2, 1)

        ax[0].set_xlabel('$t$') 
        ax[0].set_ylabel('$P_N$')
        ax[0].plot(t_g, P_N_g[:, 0], '-k', label='gen_alpha')
        ax[0].plot(t_m, P_N_m[:, 0], '--r', label='moreau')
        ax[0].legend()

        ax[1].set_xlabel('$t$') 
        ax[1].set_ylabel('$P_F$')
        ax[1].plot(t_g, P_F_g[:, 0], '-k', label='gen_alpha')
        ax[1].plot(t_m, P_F_m[:, 0], '--r', label='moreau')
        ax[1].legend()

        fig, ax = plt.subplots(1, 1)
        ax.set_title('gap computed by gen-alpha')
        ax.set_xlabel('$t$')
        ax.set_ylabel('gap')
        ax.plot(t_g, g_N[:, 0], '-k', label='$g_N^1$')
        ax.plot(t_g, g_N[:, 1], '--k', label='$g_N^2$')

        plt.show()
    