import sys
from os import path
here = path.abspath(path.dirname(__file__))

parent = path.abspath(path.dirname(path.dirname(here)))
sys.path.append(parent)

import bpy
import numpy as np
from math import pi
from blender import animate_rigid_bodies, load_data, remove_inital_cube, alpha_background, use_eevee, toggle_camera_view, set_view_point_shader

def smoothstep1(x, x_min=0, x_max=1):
    x = np.clip((x - x_min) / (x_max - x_min), 0, 1)
    return -2 * x**3 + 3 * x**2

if __name__ == "__main__":
    remove_inital_cube()
    alpha_background()
    use_eevee()
    toggle_camera_view()
    set_view_point_shader()

    my_dict = load_data(path.join(here, 'Ball_in_cylinder.npy'))

    # import cylinder
    path_cyl = path.join(here, 'Cylinder.stl')
    bpy.ops.import_mesh.stl(filepath=path_cyl, filter_glob="*.stl")
    bpy.context.active_object.scale[0] = 0.1
    bpy.context.active_object.scale[1] = 0.1
    bpy.context.active_object.scale[2] = 0.1
    # bpy.ops.object.shade_smooth()

    ####################
    # animate rigid body
    ####################
    animate_rigid_bodies(my_dict['objects'], my_dict['r_OS'], my_dict['A_IK'])

    ##################
    # set sun position
    ##################
    light1 = bpy.data.objects['Light']
    light1.location = (20, 5, 10)

    light2 = light1.copy() # duplicate linked
    light2.data = light1.data.copy() # optional: make this a real duplicate (not linked)
    bpy.context.collection.objects.link(light2) # add to scene
    light2.location = (-5, 5, 10)

    ################
    # animate camera
    ################
    initial_location = np.array((0, 1.65, 3.55))
    diff = np.array((20, 0, 0))
    cam = bpy.data.objects['Camera']
    bpy.context.scene.render.resolution_x = 1920 # set camera width
    bpy.context.scene.render.resolution_y = 1080 # set camera height
    cam.rotation_mode = 'XYZ'
    cam.rotation_euler = (-20 * pi/180, 0, 0)
    cam.location = initial_location

    n_frames = bpy.context.scene.frame_end
    # for frame in range(n_frames):
    #     bpy.context.scene.frame_set(frame)

    #     # move camera
    #     t = frame / n_frames
    #     cam.location = initial_location + smoothstep1(t, 0, 0.7) * diff
    #     cam.keyframe_insert(data_path="location", frame=frame)

    # set initial and end frame
    bpy.context.scene.frame_current = 0
    bpy.context.scene.frame_end = n_frames
