#-----------------------------------------------------------------------
# A nonsmooth generalized-alpha method for mechanical systems
# with frictional contact
# 
# Giuseppe Capobianco, Jonas Harsch, Simon R. Eugster, Remco I. Leine
#-----------------------------------------------------------------------
# Int J Numer Methods Eng. 2021; 1– 30. https://doi.org/10.1002/nme.6801
#-----------------------------------------------------------------------
# 
# This file implements the rotating bouncing ball example, 
# see Section 10.1.
#
# Stuttgart, September 2021                      G.Capobianco, J. Harsch

import sys
from os import path
here = path.abspath(path.dirname(__file__))
sys.path.append(here)

import numpy as np
from math import sin, cos, pi
import matplotlib.pyplot as plt

from PyNSGA import Generalized_alpha, Moreau

def A_IK_basic_z(phi):
    sp = sin(phi)
    cp = cos(phi)
    return np.array([[ cp, -sp, 0],\
                     [ sp,  cp, 0],\
                     [  0,   0, 1]])

class Bouncing_ball():
    def __init__(self, eN, eF, mu, q0, u0):
        
        self.m = 1
        self.R = 0.1
        self.theta = 2 / 5 * self.m * self.R**2 
        self.g_ = 9.81
        
        self.nq = 3
        self.nu = 3
        self.nla_g = 0
        self.nla_gamma = 0
        self.nla_N = 1
        self.nla_F = 1

        self.mu = mu * np.ones(self.nla_N)
        self.e_N = eN * np.ones(self.nla_N)
        self.e_F = eF * np.ones(self.nla_F)

        prox_r = 0.3
        self.prox_r_N = prox_r * np.ones(self.nla_N)
        self.prox_r_F = prox_r * np.ones(self.nla_N)

        self.NF_connectivity = [[0]]
        
        self.q0 = q0 # (x0, y0, phi0)
        self.u0 = u0 # (x_dot0, y_dot0, phi_dot0)

        self.la_g0 = np.zeros(self.nla_g)
        self.la_gamma0 = np.zeros(self.nla_gamma)

        self.la_N0 = np.zeros(self.nla_N)
        self.la_F0 = np.zeros(self.nla_F)

    def r_OS(self, t, q):
        r_OS = np.zeros(3)
        r_OS[:2] = q[:2]
        return r_OS

    def A_IK(self, t, q):
        return A_IK_basic_z(q[2])

    #####################
    # equations of motion
    #####################
    def M(self, t, q):
        return np.diag([self.m, self.m, self.theta])

    def h(self, t, q, u):
        return np.array([0,
                         - self.m * self.g_,
                         0])

    #####################
    # kinematic equations
    #####################
    def q_dot(self, t, q, u):
        return u

    def q_ddot(self, t, q, u, a):
        return a

    def B(self, t, q):
        return np.eye(self.nu)

    #######################
    # bilateral constraints
    #######################
    def g(self, t, q):
        return np.zeros(self.nla_g)

    def W_g(self, t, q):
        return np.zeros((self.nu, self.nla_g))

    def g_dot(self, t, q, u):
        return np.zeros(self.nla_g)

    def g_ddot(self, t, q, u, a):
        return np.zeros(self.nla_g)

    def gamma(self, t, q, u):
        return np.zeros(self.nla_gamma)

    def W_gamma(self, t, q):
        return np.zeros((self.nu, self.nla_gamma))

    def gamma_dot(self, t, q, u, a):
        return np.zeros(self.nla_gamma)

    #################
    # normal contacts
    #################
    def g_N(self, t, q):
        return np.array([q[1] - self.R])

    def W_N(self, t, q):
        W_N = np.zeros((self.nu, self.nla_N))
        W_N[1 , 0] = 1
        return W_N

    def g_N_dot(self, t, q, u):
        return np.array([u[1]])
    
    def xi_N(self, t, q, u_pre, u_post):
        return self.g_N_dot(t, q, u_post) + self.e_N * self.g_N_dot(t, q, u_pre)

    def g_N_ddot(self, t, q, u, a):
        return np.array([ a[1] ])

    #################
    # friction
    #################
    def gamma_F(self, t, q, u):
        x_dot, y_dot, phi_dot = u
        return np.array([ x_dot + self.R * phi_dot])

    def gamma_F_u(self, t, q):
        gamma_F_u = np.zeros((self.nla_N, self.nu))
        gamma_F_u[0 , 0] = 1
        gamma_F_u[0 , 2] = self.R
        return gamma_F_u

    def W_F(self, t, q):
        return self.gamma_F_u(t, q).T

    def gamma_F_dot(self, t, q, u, a):
        x_ddot, y_ddot, phi_ddot = a
        return np.array([ x_ddot + self.R * phi_ddot])

    def xi_F(self, t, q, u_pre, u_post):
        return self.gamma_F(t, q, u_post) + self.e_F * self.gamma_F(t, q, u_pre)
    
if __name__ == "__main__":
    
    # create output data for plots in paper
    output_paper = True
    
    # create output data for blender visualization
    output_blender = True
    
    # cases of system parameters (case 1,2,3 of paper)
    # case 1: Figure 1
    # case 2: Figure 2 (left)
    # case 3: Figure 2 (right)
    case = 3

    # show simulation results
    show_plots = True

    if case == 1:
        eN, eF, mu = 0.5, 0, 0.2
        q0 = np.array([0, 1, 0])
        u0 = np.array([0, 0, 0])
        t1 = 1.5
    elif case == 2:
        eN, eF, mu = 0, 0, 0.2
        q0 = np.array([0, 1, 0])
        u0 = np.array([0, 0, 50])
        t1 = 1.1
    elif case == 3:
        eN, eF, mu = 0, 0, 0.2
        q0 = np.array([0, 1, 0])
        u0 = np.array([0, 0, 10])
        t1 = 1.1
    elif case == 4:
        # more complex example for benchmarking purposes
        eN, eF, mu = 0.5, 0, 0.2
        q0 = np.array([-0.5, 1, 0])
        u0 = np.array([1, 0, 50])
        t1 = 1.5
    else:
        raise AssertionError('Case not found!')


    ball = Bouncing_ball(eN, eF, mu, q0, u0)

    t0 = 0
    dt = 2e-3
    gen_al = Generalized_alpha(ball, t0, t1, dt, rho_inf=0.5, newton_tol=1.0e-6)    
    sol_g = gen_al.solve()

    t_g = sol_g[0]
    q_g = sol_g[1]
    u_g = sol_g[2]
    a_g = sol_g[3]
    La_N_g = sol_g[10]
    la_N_g = sol_g[11]
    La_F_g = sol_g[12]
    la_F_g = sol_g[13]
    P_N_g = sol_g[14]
    P_F_g = sol_g[15]

    moreau = Moreau(ball, t0, t1, dt, fix_point_tol=1e-8)
    sol_m = moreau.solve()
    t_m = sol_m[0]
    q_m = sol_m[1]
    u_m = sol_m[2]
    P_N_m = sol_m[5]
    P_F_m = sol_m[6]

    if output_blender:
        nt = len(t_g)
        r_OS = np.zeros((nt, 3))
        A_IK = np.zeros((nt, 3, 3))

        for i, (ti, qi) in enumerate(zip(t_g, q_g)):
            r_OS[i] = ball.r_OS(ti, qi) 
            A_IK[i] = ball.A_IK(ti, qi)

        n_frames = 200
        frames = np.linspace(0, nt - 1, num=n_frames, dtype=int)

        my_dict = {
            'objects': [path.join(here, 'Ball.stl')],
            'r_OS': [r_OS[frames]],
            'A_IK': [A_IK[frames]],
            't': t_g[frames],
            'dt': dt,
        }
        np.save(path.join(here, 'Bouncing_ball.npy'), my_dict, allow_pickle=True)

    if output_paper:
        if case == 1 or case == 4:
            txt_data = np.vstack([t_g, q_g[:, 1] - ball.R])
            np.savetxt(path.join(here, 'bouncing_ball_accumulation.txt'), txt_data.T)
        
        if case == 2:
            txt_data = np.vstack([t_g, la_F_g[:, 0]])
            np.savetxt(path.join(here, 'bouncing_ball_la_nonimp_F_g_case2.txt'), txt_data.T)
            txt_data = np.vstack([t_g, La_F_g[:, 0]])
            np.savetxt(path.join(here, 'bouncing_ball_la_imp_F_g_case2.txt'), txt_data.T)
        
        if case == 3:
            txt_data = np.vstack([t_g, la_F_g[:, 0]])
            np.savetxt(path.join(here, 'bouncing_ball_la_nonimp_F_g_case3.txt'), txt_data.T)
            txt_data = np.vstack([t_g, La_F_g[:, 0]])
            np.savetxt(path.join(here, 'bouncing_ball_la_imp_F_g_case3.txt'), txt_data.T)

    if show_plots:
        
        fig, ax = plt.subplots(3, 1)

        ax[0].set_xlabel('$t$')
        ax[0].set_ylabel('$x$')
        ax[0].plot(t_g, q_g[:, 0], '-k', label='gen_alpha')
        ax[0].plot(t_m, q_m[:, 0], '--r', label='moreau')
        ax[0].legend()

        ax[1].set_xlabel('$t$')
        ax[1].set_ylabel('$u_x$')
        ax[1].plot(t_g, u_g[:, 0], '-k', label='gen_alpha')
        ax[1].plot(t_m, u_m[:, 0], '--r', label='moreau')
        ax[1].legend()

        ax[2].set_xlabel('$t$')
        ax[2].set_ylabel('$a_x$')
        ax[2].plot(t_g, a_g[:, 0], '-k', label='gen_alpha')
        ax[2].legend()
        
        ######
        fig, ax = plt.subplots(3, 1)

        ax[0].set_xlabel('$t$')
        ax[0].set_ylabel('$y$')
        ax[0].plot(t_g, q_g[:, 1], '-k', label='gen_alpha')
        ax[0].plot(t_m, q_m[:, 1], '--r', label='moreau')
        ax[0].legend()

        ax[1].set_xlabel('$t$')
        ax[1].set_ylabel('$u_y$')
        ax[1].plot(t_g, u_g[:, 1], '-k', label='gen_alpha')
        ax[1].plot(t_m, u_m[:, 1], '--r', label='moreau')
        ax[1].legend()

        ax[2].set_xlabel('$t$')
        ax[2].set_ylabel('$a_y$')
        ax[2].plot(t_g, a_g[:, 1], '-k', label='gen_alpha')
        ax[2].legend()
        
        ######
        fig, ax = plt.subplots(3, 1)

        ax[0].set_xlabel('$t$')
        ax[0].set_ylabel(r'$\varphi$')
        ax[0].plot(t_g, q_g[:, 2], '-k', label='gen_alpha')
        ax[0].plot(t_m, q_m[:, 2], '--r', label='moreau')
        ax[0].legend()

        ax[1].set_xlabel('$t$')
        ax[1].set_ylabel(r'$u_\varphi$')
        ax[1].plot(t_g, u_g[:, 2], '-k', label='gen_alpha')
        ax[1].plot(t_m, u_m[:, 2], '--r', label='moreau')
        ax[1].legend()

        ax[2].set_xlabel('$t$')
        ax[2].set_ylabel(r'$a_\varphi$')
        ax[2].plot(t_g, a_g[:, 2], '-k', label='gen_alpha')
        ax[2].legend()

        ######
        fig, ax = plt.subplots(2, 1)
        ax[0].set_title('force comp. by gen-alpha')
        ax[0].set_xlabel('$t$')
        ax[0].set_ylabel('force')
        ax[0].plot(t_g, la_N_g[:, 0], '-b', label='$\lambda_N$')
        ax[0].plot(t_g, La_N_g[:, 0], '--r', label='$\Lambda_N$')
        ax[0].legend()

        ax[1].set_xlabel('$t$')
        ax[1].set_ylabel('force')
        ax[1].plot(t_g, la_F_g[:, 0], '-b', label='$\lambda_F$')
        ax[1].plot(t_g, La_F_g[:, 0], '--r', label='$\Lambda_F$')
        ax[1].legend()

        ######
        fig, ax = plt.subplots(2, 1)

        ax[0].set_xlabel('$t$')
        ax[0].set_ylabel('$P_N$')
        ax[0].plot(t_g, P_N_g[:, 0], '-k', label='gen_alpha')
        ax[0].plot(t_m, P_N_m[:, 0], '--r', label='moreau')
        ax[0].legend()

        ax[1].set_xlabel('$t$')
        ax[1].set_ylabel('$P_F$')
        ax[1].plot(t_g, P_F_g[:, 0], '-k', label='gen_alpha')
        ax[1].plot(t_m, P_F_m[:, 0], '--r', label='moreau')
        ax[1].legend()

    plt.show()