from setuptools import setup, find_packages

setup(
    name='PyNSGA',
    version='1.0.0',
    author='Giuseppe Capobianco, Jonas Harsch',
    author_email='capobianco@inm.uni-stuttgart.de, harsch@inm.uni-stuttgart.de',
    description='Implementation of the nonsmooth generalized-alpha scheme.',
    long_description='Implementation of the scheme presented in ''''A nonsmooth generalized-alpha method for mechanical systems with frictional contact'''' by Giuseppe Capobianco, Jonas Harsch, Simon R. Eugster and Remco I. Leine, which is published in Int J Numer Methods Eng. 2021; 1– 30. https://doi.org/10.1002/nme.6801.',
    install_requires=[
        'numpy>=1.18.3', 
        'scipy>=1.4.1', 
        'matplotlib>=3.2.1', 
        'tqdm>=4.46.0',],
    packages=find_packages(),
    python_requires='>=3.8',
)